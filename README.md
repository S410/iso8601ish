# ISO8601-ish Clock

![Screenshot](media/screenshot.png)

### Get on [extensions.gnome.org](https://extensions.gnome.org/extension/6413/iso8601-ish-clock/)

> For **Gnome 45**

Why -ish? Because I'm using a space instead of a T
